Narrative:
In order the check the performance of carrier
As a transportation analyst
I want to calculate the performance result


Scenario: Performance calculation - late delivery
Given a cmr is shipped on 2015-01-01
And an expected delivery date of 2015-01-10
When a delivered edi event is received with event date time 2015-01-12
Then the performance result type must be LATE
And the responsible must be NIKE

Scenario: Performance calculation - early delivery
Given a cmr is shipped on 2015-01-01
And an expected delivery date of 2015-01-10
When a delivered edi event is received with event date time 2015-01-09
Then the performance result type must be EARLY
And the responsible must be CARRIER