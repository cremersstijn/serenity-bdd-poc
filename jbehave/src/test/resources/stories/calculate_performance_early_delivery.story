Narrative:
In order the check the performance of carrier
As a transportation analyst
I want to see if a carrier has delivered early


Scenario: Performance calculation - early delivery
Given a cmr is shipped on 2015-01-01
And an expected delivery date of 2015-01-10
When a delivered edi event is received with event date time 2015-01-09
Then the performance result type must be EARLY