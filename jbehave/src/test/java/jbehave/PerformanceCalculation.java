package jbehave;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import static org.assertj.core.api.Assertions.*;

public class PerformanceCalculation {


    @Given("a cmr is shipped on $shipDate")
    public void setupCmr(String shipDate){

    }

    @Given("an expected delivery date of $edd")
    public void setupEdd(String edd){

    }

    @When("a delivered edi event is received with event date time $eventDateTime")
    public void receiveDeliveredEdiEvent(String eventDateTime){

    }

    @Then("the performance result type must be $type")
    public void verifyPerfResult(String type){
//        Assert.assertEquals(type, "LATE");
        String actual = "LATE";
        assertThat(actual).isEqualTo(type);
    }

    @Then("the responsible must be $type")
    public void verifyResponsible(String type){
        //        Assert.assertEquals(type, "LATE");
        String actual = "NIKE";
        assertThat(actual).isEqualTo(type);
    }
}
